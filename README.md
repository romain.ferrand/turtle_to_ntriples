# Turtle_to_ntriples

Author
-----------
Romain Ferrand


What is it?
-----------
A transcompiler from turtle to ntriples format


How to use it?
-----------
Compile with: 
make

launch a test example with:
./main.native < ../tests/test1.ttl

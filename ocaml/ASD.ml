(* ASD type *)
type entity =
    Ent of string
and obj =
    Ent_obj of entity
  | Id of string
and predicat =
  {
    pred_id : entity;
    objects : obj list
  }
and subject =
  {
    subject_id : entity;
    predicats : predicat list
  }
type turtle = Turtle of subject list
type ntriple =
  {n_subject:entity; n_predicat:entity; n_obj: obj}

  (* Fill here! *)
(* Function to print the AST *)
(*TODO: tail-rec pretty printing*)
let entity_to_string_ast = function
    Ent(str) -> "Ent("^str^")"
let entity_to_string = function
  Ent(str) -> "<"^str^">"
let object_to_string_ast = function
    Ent_obj(ent) -> "Ent_obj("^entity_to_string_ast ent ^")"
  | Id(str) -> "ID("^str^")"
let object_to_string = function
    Ent_obj(ent) -> entity_to_string ent
  |Id(str) -> "\""^str^"\""
let rec objects_to_string_ast = function
    h::t -> let res = (object_to_string_ast h) in
    if(t == []) then res else (res^", "^(objects_to_string_ast t))
  |[] -> ""
let predicat_to_string_ast pred =
  "Pred(pred_id="^(entity_to_string_ast pred.pred_id)^",objects=["^(objects_to_string_ast pred.objects)^"])"
let rec predicats_to_string_ast = function
    h::t -> let res = (predicat_to_string_ast h)
    in if (t == []) then res else (res^", "^predicats_to_string_ast t)
  |[] -> ""
let subject_to_string_ast sub =
  "Subject(subject_id="^(entity_to_string_ast sub.subject_id)^", predicats=["^(predicats_to_string_ast sub.predicats)^"])"
let rec subjects_to_string_ast = function
    h::t -> let res = (subject_to_string_ast h) in
    if (t == []) then res else res^", "^(subjects_to_string_ast t)
  |[] -> ""
let turtle_to_string_ast = function
    Turtle(exp) -> "Turtle("^(subjects_to_string_ast exp)^")"
(* Function to generate the document out of the AST *)
let ntriples_to_string ntrpl =
  (entity_to_string ntrpl.n_subject)^" "^(entity_to_string ntrpl.n_predicat)^" "^(object_to_string ntrpl.n_obj)^" ."
let object_to_ntriples sub pred obj =
  {n_subject=sub; n_predicat=pred; n_obj=obj}

let rec objects_to_ntriples sub pred = function
  h::t -> (object_to_ntriples sub pred h)::(objects_to_ntriples sub pred t)
  |[] -> []
let predicat_to_ntriples sub pred =
  (objects_to_ntriples sub pred.pred_id pred.objects)
let rec predicats_to_ntriples sub = function
  |h::t -> (predicat_to_ntriples sub h)@(predicats_to_ntriples sub t)
  |[] -> []
let subject_to_ntriples sub =
  (predicats_to_ntriples sub.subject_id sub.predicats)
let rec subjects_to_ntriples = function
  |h::t -> (subject_to_ntriples h)@(subjects_to_ntriples t)
  |[] -> []
let turtle_to_ntriples = function
  Turtle(exp) -> (subjects_to_ntriples exp)
let ast_to_ntriples ast =
  let l = turtle_to_ntriples ast in
  let rec aux = function
      h::t -> ntriples_to_string h^"\n"^aux t
    |[] -> ""
  in aux l

